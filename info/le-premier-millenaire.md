Une créature nommée l'Anathème est apparue dans le monde, détruisant tout.
Les humains et les nains se sont vu pourvoir d'armes, les "anges", créations des enfers.
Les enfers ont également lâchés sur le monde une autre créature pour détruire l'Anathème.
L'Anathème est alors vaincue, mais pas détruite.

On estime cela au tout début du premier millénaire, il y a 3000 ans.
Une bataille majeure a sûrement eu lieu au sud des montagnes de [[Ceo-Guth]], à l'emplacement de la ville [[Sai-Rae]].

- **source: un manuscrit de "Etienne Malzeu"**
- **source: rapport d'enquête des [[chasseurs de l'aurore]]**

---

Une prédiction dit que "L'empire sera détruit un 25 Ventôse".

**source: un drow, il y a 500 ans, Ezekiel**
