# 22 Ventôse
On quitte les autres.

# 23 Ventôse
Rencontre tôt le matin, de nuit, avec les fée et leur chef.

# 24 Ventôse

# 25 Ventôse
Festival. Vol de la clé, prise de connaissance avec Margerite Amboise. Récupération d'indice matériel sur les soldas mécaniques illégaux.

# 26 Ventôse
Création de contrefaçons. Une rencontre entre le Baron et un négociant d'Amboise à propos de soldat mécanique détruit. Tentative de prise de contact avec Margerite en privée mais on échoue.

# 27 Ventôse
Bob apprend "invisiblité", et discute avec le Baron pour préparer notre faux arrangement. Rowena teste la clé. On passe un pacte avec un Dieu dans un monde désertique (et évoque des abysses) doté de 5 soleils.
