## 5 Ventôse 3004

*arrivée à Vigrid*

## 6 Ventôse

*rencontre avec Pascal et la bande*

## 11 Ventôse

*execution de Lazarus et recherche en bibliotèque*

## 20 Ventôse

*arrivée à la taverne de Merigan*

## 21 Ventôse

*rencontre avec Evan l'usurpateur*

## 22 Ventôse

*visite à la mine, rencontre et combat avec les fées des rôches*