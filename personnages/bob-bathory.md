# Bartholomet Octavius Gaspard Balthazar Melchior von Bathory

Dhampir né de manière prématuré suite à la mort de sa mère, mordue par son père [[Magnus Bathory]], vampirisé et devenu fou. Seul héritier de son domaine, il est chassé par son oncle avec sa nourrice. Il devient alors mage à [[Vigrid]], et finit par partir à l'aventure après de longues décennies d'études.

Il retrouve sa nourrice, réincarnée en [[Rowena]]. Après quelques années d'aventures et de batailles, il trouve [[Grudu]] le barbare orc. L'équipe se rend peu après à Walt, où ils sont confronté à [[Lazarus]] et une armée de créature impies. Il y gagne une marque surnaturelle posé par une entité qui se dit divine et capable de ressusciter [[Rowena]] après qu'elle ai été tuée au combat.

Après avoir convaincu un groupe de bandit de le rejoindre pour être aventurier, il va à [[Vigrid]] où il recrute d'anciens amis magicien pour l'accompagner.
Il vont ensuite tous vers son ancien domaine, car il souhaite y installer sa guilde en cours de formation.
Avec sa guilde il y trouve des preuves de contrats illégaux entre [[Evan Bathory]] et la pègre de [[Margerite Amboise]], et ainsi il devient le nouveau propriétaire du domaine, son manoir et sa forêt.